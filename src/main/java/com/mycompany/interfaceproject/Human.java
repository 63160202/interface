/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceproject;

/**
 *
 * @author admin
 */
public class Human extends LandAnimal {
    private String nickname;

    public Human(String nickname) {
        super("Human", 2);
        this.nickname = nickname;
    }

    @Override
    public void run() {
        System.out.println("Human : " + nickname + " can run");
    }

    @Override
    public void eat() {
        System.out.println("Human : " + nickname + " can eat");
    } 

    @Override
    public void speak() {
        System.out.println("Human : " + nickname + " can speak");
    }

    @Override
    public void sleep() {
        System.out.println("Human : " + nickname + " can sleep");
    }

}

