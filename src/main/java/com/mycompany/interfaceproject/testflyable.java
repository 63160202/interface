/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.interfaceproject;

/**
 *
 * @author admin
 */
public class testflyable {

    public static void main(String[] args) {

        bat bt1 = new bat("linmo");
        bt1.fly();

        Plane pl1 = new Plane("Engine Number 1");
        pl1.fly();

        dog d1 = new dog("jeno");
        d1.run();

        car cr1 = new car("Engine Number 2");
        cr1.run();

        Human h1 = new Human("lucky");
        h1.run();

        flyable[] flyable = {pl1, bt1};
        runable[] runable = {d1, pl1, cr1, h1};

        System.out.println("===============================================");
        System.out.println("runable");
        flyable[] flyables = {bt1, pl1};
        for (int i = 0; i < runable.length; i++) {
            if (runable[i] instanceof Plane) {
                ((Plane) (runable[i])).startEngine();
                ((Plane) (runable[i])).raiseSpeed();
                ((Plane) (runable[i])).run();
                ((Plane) (runable[i])).fly();

            } else {
                runable[i].run();
            }
        }
        System.out.println("===============================================");
        System.out.println("Flyable");
        for (int i = 0; i < flyable.length; i++) {
            if (flyable[i] instanceof Plane) {
                ((Plane) (flyable[i])).startEngine();
                ((Plane) (flyable[i])).raiseSpeed();
                ((Plane) (flyable[i])).run();
                ((Plane) (flyable[i])).fly();
            } else {
                flyable[i].fly();
            }
        }
    }
}
